# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialogloaderror.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DialogLoadError(object):
    def setupUi(self, DialogLoadError):
        DialogLoadError.setObjectName("DialogLoadError")
        DialogLoadError.resize(460, 99)
        DialogLoadError.setModal(True)
        self.buttonBox = QtWidgets.QDialogButtonBox(DialogLoadError)
        self.buttonBox.setGeometry(QtCore.QRect(80, 60, 371, 23))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.lineEditImagePath = QtWidgets.QLineEdit(DialogLoadError)
        self.lineEditImagePath.setGeometry(QtCore.QRect(100, 20, 261, 20))
        self.lineEditImagePath.setObjectName("lineEditImagePath")
        self.pushButtonBrowse = QtWidgets.QPushButton(DialogLoadError)
        self.pushButtonBrowse.setGeometry(QtCore.QRect(370, 20, 81, 23))
        self.pushButtonBrowse.setObjectName("pushButtonBrowse")
        self.label = QtWidgets.QLabel(DialogLoadError)
        self.label.setGeometry(QtCore.QRect(10, 20, 81, 16))
        self.label.setObjectName("label")

        self.retranslateUi(DialogLoadError)
        self.buttonBox.accepted.connect(DialogLoadError.accept)
        self.buttonBox.rejected.connect(DialogLoadError.reject)
        QtCore.QMetaObject.connectSlotsByName(DialogLoadError)
        DialogLoadError.setTabOrder(self.lineEditImagePath, self.pushButtonBrowse)
        DialogLoadError.setTabOrder(self.pushButtonBrowse, self.buttonBox)

    def retranslateUi(self, DialogLoadError):
        _translate = QtCore.QCoreApplication.translate
        DialogLoadError.setWindowTitle(_translate("DialogLoadError", "Image not found"))
        self.pushButtonBrowse.setText(_translate("DialogLoadError", "Browse..."))
        self.label.setText(_translate("DialogLoadError", "Image path"))

