# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'dialogproperties.ui'
#
# Created by: PyQt5 UI code generator 5.10.1
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets

class Ui_DialogProperties(object):
    def setupUi(self, DialogProperties):
        DialogProperties.setObjectName("DialogProperties")
        DialogProperties.resize(426, 340)
        self.gridLayout = QtWidgets.QGridLayout(DialogProperties)
        self.gridLayout.setObjectName("gridLayout")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.buttonBox = QtWidgets.QDialogButtonBox(DialogProperties)
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtWidgets.QDialogButtonBox.Cancel|QtWidgets.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.gridLayout_2.addWidget(self.buttonBox, 3, 0, 1, 1)
        self.groupBox_Style = QtWidgets.QGroupBox(DialogProperties)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox_Style.sizePolicy().hasHeightForWidth())
        self.groupBox_Style.setSizePolicy(sizePolicy)
        self.groupBox_Style.setObjectName("groupBox_Style")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.groupBox_Style)
        self.verticalLayout.setSizeConstraint(QtWidgets.QLayout.SetDefaultConstraint)
        self.verticalLayout.setObjectName("verticalLayout")
        self.formLayout = QtWidgets.QFormLayout()
        self.formLayout.setFieldGrowthPolicy(QtWidgets.QFormLayout.AllNonFixedFieldsGrow)
        self.formLayout.setObjectName("formLayout")
        self.label = QtWidgets.QLabel(self.groupBox_Style)
        self.label.setObjectName("label")
        self.formLayout.setWidget(1, QtWidgets.QFormLayout.LabelRole, self.label)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.horizontalSlider_Transparency = QtWidgets.QSlider(self.groupBox_Style)
        self.horizontalSlider_Transparency.setMaximum(100)
        self.horizontalSlider_Transparency.setOrientation(QtCore.Qt.Horizontal)
        self.horizontalSlider_Transparency.setObjectName("horizontalSlider_Transparency")
        self.horizontalLayout_3.addWidget(self.horizontalSlider_Transparency)
        self.spinBox_Transparency = QtWidgets.QSpinBox(self.groupBox_Style)
        self.spinBox_Transparency.setMaximum(100)
        self.spinBox_Transparency.setObjectName("spinBox_Transparency")
        self.horizontalLayout_3.addWidget(self.spinBox_Transparency)
        self.label_4 = QtWidgets.QLabel(self.groupBox_Style)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_3.addWidget(self.label_4)
        self.formLayout.setLayout(1, QtWidgets.QFormLayout.FieldRole, self.horizontalLayout_3)
        self.verticalLayout.addLayout(self.formLayout)
        self.gridLayout_2.addWidget(self.groupBox_Style, 2, 0, 1, 1)
        self.groupBox_Properties = QtWidgets.QGroupBox(DialogProperties)
        self.groupBox_Properties.setObjectName("groupBox_Properties")
        self.gridLayout_3 = QtWidgets.QGridLayout(self.groupBox_Properties)
        self.gridLayout_3.setObjectName("gridLayout_3")
        self.textEdit_Properties = QtWidgets.QTextEdit(self.groupBox_Properties)
        self.textEdit_Properties.setReadOnly(True)
        self.textEdit_Properties.setTabStopWidth(80)
        self.textEdit_Properties.setObjectName("textEdit_Properties")
        self.gridLayout_3.addWidget(self.textEdit_Properties, 0, 0, 1, 1)
        self.gridLayout_2.addWidget(self.groupBox_Properties, 0, 0, 1, 1)
        self.gridLayout.addLayout(self.gridLayout_2, 0, 0, 1, 1)

        self.retranslateUi(DialogProperties)
        self.buttonBox.accepted.connect(DialogProperties.accept)
        self.buttonBox.rejected.connect(DialogProperties.reject)
        QtCore.QMetaObject.connectSlotsByName(DialogProperties)

    def retranslateUi(self, DialogProperties):
        _translate = QtCore.QCoreApplication.translate
        DialogProperties.setWindowTitle(_translate("DialogProperties", "Properties"))
        self.groupBox_Style.setTitle(_translate("DialogProperties", "Style"))
        self.label.setText(_translate("DialogProperties", "Transparency"))
        self.label_4.setText(_translate("DialogProperties", "%"))
        self.groupBox_Properties.setTitle(_translate("DialogProperties", "Properties"))

