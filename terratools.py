# -*- coding: utf-8 -*-

import os.path

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import QAction
from qgis.core import QgsApplication, QgsMapLayer, QgsProject, Qgis
from qgis.utils import showPluginHelp

from . import resources_rc  # noqa
from . import utils

from .terratools_commands import ExportGeorefRasterCommand
from .terratools_layer import (
    TRasterLayerType, TRasterLayer)
from .terratools_maptools import \
    (MoveRasterMapTool,
     RotateRasterMapTool,
     ScaleRasterMapTool,
     AdjustRasterMapTool,
     BarRescaleRasterMapTool,
     GeorefRasterBy2PointsMapTool,
     PointIncrementTool,
     CoordFromPointsTool,
     CreateGridTool,
     CreateTransectTool,
     ImportFileTool)
from .dialogaddimage import (
    AddImageDialog)
from .dialogexportgeorast import (
    ExportGeoRastDialog)


class TerraTools(object):

    PLUGIN_MENU = "&Terra Tools"

    def __init__(self, iface):
        self.iface = iface
        self.plugin_dir = os.path.dirname(__file__)
        self.layers = {}
        QgsProject.instance().layerRemoved.connect(self.layerRemoved)
        self.iface.currentLayerChanged.connect(
            self.currentLayerChanged)

    def initGui(self):
        # Create actions
        self.actionAddLayer = QAction(
            QIcon(":/plugins/terratools/iconAdd.png"),
            "Add Raster",
            self.iface.mainWindow())
        self.actionAddLayer.setObjectName(
            "TerraToolsPlugin_AddLayer")
        self.actionAddLayer.triggered.connect(self.addLayer)

        self.actionMoveRaster = QAction(
            QIcon(":/plugins/terratools/iconMove.png"),
            "Move Raster",
            self.iface.mainWindow())
        self.actionMoveRaster.setObjectName(
            "TerraToolsPlugin_MoveRaster")
        self.actionMoveRaster.triggered.connect(self.moveRaster)
        self.actionMoveRaster.setCheckable(True)

        self.actionRotateRaster = QAction(
            QIcon(":/plugins/terratools/iconRotate.png"),
            "Rotate Raster", self.iface.mainWindow())
        self.actionRotateRaster.setObjectName(
            "TerraToolsPlugin_RotateRaster")
        self.actionRotateRaster.triggered.connect(self.rotateRaster)
        self.actionRotateRaster.setCheckable(True)

        self.actionScaleRaster = QAction(
            QIcon(":/plugins/terratools/iconScale.png"),
            "Scale Raster", self.iface.mainWindow())
        self.actionScaleRaster.setObjectName(
            "TerraToolsPlugin_ScaleRaster")
        self.actionScaleRaster.triggered.connect(self.scaleRaster)
        self.actionScaleRaster.setCheckable(True)

        self.actionAdjustRaster = QAction(
            QIcon(":/plugins/terratools/iconAdjust.png"),
            "Adjust Raster Sides", self.iface.mainWindow())
        self.actionAdjustRaster.setObjectName(
            "TerraToolsPlugin_AdjustRaster")
        self.actionAdjustRaster.triggered.connect(self.adjustRaster)
        self.actionAdjustRaster.setCheckable(True)

        self.actionBarRescaleRaster = QAction(
            QIcon(":/plugins/terratools/iconBarRescale.png"),
            "Rescale Raster With Map Scale", self.iface.mainWindow())
        self.actionBarRescaleRaster.setObjectName(
            "TerraToolsPlugin_BarRescaleRaster")
        self.actionBarRescaleRaster.triggered.connect(self.barRescaleRaster)
        self.actionBarRescaleRaster.setCheckable(True)

        self.actionGeoref2PRaster = QAction(
            QIcon(":/plugins/terratools/icon2Points.png"),
            "Two Point Raster Georeference", self.iface.mainWindow())
        self.actionGeoref2PRaster.setObjectName(
            "TerraToolsPlugin_Georef2PRaster")
        self.actionGeoref2PRaster.triggered.connect(self.georef2PRaster)
        self.actionGeoref2PRaster.setCheckable(True)

        self.actionIncreaseTransparency = QAction(
            QIcon(":/plugins/terratools/"
                  "iconTransparencyIncrease.png"),
            "Increase Transparency", self.iface.mainWindow())
        self.actionIncreaseTransparency.triggered.connect(
            self.increaseTransparency)
        self.actionIncreaseTransparency.setShortcut("Alt+Ctrl+L")

        self.actionDecreaseTransparency = QAction(
            QIcon(":/plugins/terratools/"
                  "iconTransparencyDecrease.png"),
            "Decrease Transparency", self.iface.mainWindow())
        self.actionDecreaseTransparency.triggered.connect(
            self.decreaseTransparency)
        self.actionDecreaseTransparency.setShortcut("Alt+Ctrl+K")

        self.actionUndo = QAction(
            QIcon(":/plugins/terratools/iconUndo.png"),
            "Undo", self.iface.mainWindow())
        self.actionUndo.triggered.connect(self.undo)

        self.actionExport = QAction(
            QIcon(":/plugins/terratools/iconExport.png"),
            "Export Georeferenced Raster", self.iface.mainWindow())
        self.actionExport.triggered.connect(self.exportGeorefRaster)

        self.actionPointIncrement = QAction(
            QIcon(":/plugins/terratools/iconIncrement.png"),
            "Add Autoincrementing Points", self.iface.mainWindow())
        self.actionPointIncrement.setObjectName(
            "TerraToolsPlugin_PointIncrement")
        self.actionPointIncrement.triggered.connect(self.pointIncrement)
        self.actionPointIncrement.setCheckable(True)

        self.actionCoordFromPoints = QAction(
            QIcon(":/plugins/terratools/iconCoordPoints.png"),
            "Extract Coordinates From Points",
            self.iface.mainWindow())
        self.actionCoordFromPoints.setObjectName(
            "TerraToolsPlugin_CoordFromPoints")
        self.actionCoordFromPoints.triggered.connect(self.coordFromPoints)

        self.actionCreateGrid = QAction(
            QIcon(":/plugins/terratools/iconGrid.png"),
            "Create Local Grid", self.iface.mainWindow())
        self.actionCreateGrid.setObjectName(
            "TerraToolsPlugin_CreateGrid")
        self.actionCreateGrid.triggered.connect(self.createGrid)
        self.actionCreateGrid.setCheckable(True)
        # Grid tool creates layers, instead of editting, always enable
        self.actionCreateGrid.setEnabled(True)

        self.actionCreateTransect = QAction(
            QIcon(":/plugins/terratools/iconTransect.png"),
            "Create Point Transect", self.iface.mainWindow())
        self.actionCreateTransect.setObjectName(
            "TerraToolsPlugin_CreateTransect")
        self.actionCreateTransect.triggered.connect(self.createTransect)
        self.actionCreateTransect.setCheckable(True)

        self.actionImportFile = QAction(
            QIcon(":/plugins/terratools/iconCSV.png"),
            "Import CSV Data for Manual Placement", self.iface.mainWindow())
        self.actionImportFile.setObjectName(
            "TerraToolsPlugin_ImportFile")
        self.actionImportFile.triggered.connect(self.importFile)
        self.actionImportFile.setCheckable(True)

        self.actionOpenHelp = QAction(
            QIcon(":/plugins/terratools/iconHelp.png"),
            "Open Help",
            self.iface.mainWindow())
        self.actionOpenHelp.setObjectName(
            "TerraToolsPlugin_OpenHelp")
        self.actionOpenHelp.triggered.connect(self.openHelp)
        #self.actionOpenHelp.setEnabled(True)

        # # Add toolbar button and menu item for AddLayer
        # self.iface.layerToolBar().addAction(self.actionAddLayer)
        # self.iface.insertAddLayerAction(self.actionAddLayer)
        # self.iface.addPluginToRasterMenu(
        #     TerraTools.PLUGIN_MENU, self.actionAddLayer)

        # create toolbar for this plugin
        self.toolbar = self.iface.addToolBar("Terra Tools")
        self.toolbar.addAction(self.actionAddLayer)
        self.toolbar.addAction(self.actionExport)
        self.toolbar.addAction(self.actionMoveRaster)
        self.toolbar.addAction(self.actionRotateRaster)
        self.toolbar.addAction(self.actionScaleRaster)
        self.toolbar.addAction(self.actionAdjustRaster)
        self.toolbar.addAction(self.actionBarRescaleRaster)
        self.toolbar.addAction(self.actionGeoref2PRaster)
        self.toolbar.addAction(self.actionDecreaseTransparency)
        self.toolbar.addAction(self.actionIncreaseTransparency)
        self.toolbar.addAction(self.actionUndo)
        self.toolbar.addSeparator()
        self.toolbar.addAction(self.actionPointIncrement)
        self.toolbar.addAction(self.actionCreateTransect)
        self.toolbar.addAction(self.actionCoordFromPoints)
        self.toolbar.addAction(self.actionImportFile)
        self.toolbar.addAction(self.actionCreateGrid)
        #extract from CSV / .txt tool
        self.toolbar.addSeparator()
        self.toolbar.addAction(self.actionOpenHelp)

        # Register plugin layer type
        self.layerType = TRasterLayerType(self)
        QgsApplication.pluginLayerRegistry().addPluginLayerType(self.layerType)

        self.dialogAddLayer = AddImageDialog()
        self.dialogExportGeorefRaster = ExportGeoRastDialog()

        self.moveTool = MoveRasterMapTool(self.iface)
        self.moveTool.setAction(self.actionMoveRaster)
        self.rotateTool = RotateRasterMapTool(self.iface)
        self.rotateTool.setAction(self.actionRotateRaster)
        self.scaleTool = ScaleRasterMapTool(self.iface)
        self.scaleTool.setAction(self.actionScaleRaster)
        self.adjustTool = AdjustRasterMapTool(self.iface)
        self.adjustTool.setAction(self.actionAdjustRaster)
        self.barRescaleTool = BarRescaleRasterMapTool(self.iface)
        self.barRescaleTool.setAction(self.actionBarRescaleRaster)
        self.georef2PTool = GeorefRasterBy2PointsMapTool(self.iface)
        self.georef2PTool.setAction(self.actionGeoref2PRaster)
        self.incrementTool = PointIncrementTool(self.iface)
        self.incrementTool.setAction(self.actionPointIncrement)
        self.coordPointsTool = CoordFromPointsTool(self.iface)
        self.coordPointsTool.setAction(self.actionCoordFromPoints)
        self.createGridTool = CreateGridTool(self.iface)
        self.createGridTool.setAction(self.actionCreateGrid)
        self.createTransectTool = CreateTransectTool(self.iface)
        self.createTransectTool.setAction(self.actionCreateTransect)
        self.importFileTool = ImportFileTool(self.iface)
        self.importFileTool.setAction(self.actionImportFile)
        self.currentTool = None

        # default state for toolbar
        self.checkCurrentLayerIsPluginLayer()

    def unload(self):
        # Remove the plugin menu item and icon
        self.iface.layerToolBar().removeAction(self.actionAddLayer)
        self.iface.removeAddLayerAction(self.actionAddLayer)
        self.iface.removePluginRasterMenu(
            TerraTools.PLUGIN_MENU, self.actionAddLayer)

        # Unregister plugin layer type
        QgsApplication.pluginLayerRegistry().removePluginLayerType(
            TRasterLayer.LAYER_TYPE)

        QgsProject.instance().layerRemoved.disconnect(self.layerRemoved)
        self.iface.currentLayerChanged.disconnect(
            self.currentLayerChanged)

        del self.toolbar

    def layerRemoved(self, layerId):
        if layerId in self.layers:
            del self.layers[layerId]
            self.checkCurrentLayerIsPluginLayer()

    def currentLayerChanged(self, layer):
        self.checkCurrentLayerIsPluginLayer()

    #grey out tools if editable layer not selected
    def checkCurrentLayerIsPluginLayer(self):
        layer = self.iface.activeLayer()

        # Deactivate current tool when changing layer
        if self.currentTool:
            self.currentTool.reset()
            self.currentTool.setLayer(None)
            self.iface.mapCanvas().unsetMapTool(self.currentTool)
            self.iface.actionPan().trigger()
            self.currentTool = None

        if (layer and
            layer.type() == QgsMapLayer.PluginLayer and
                layer.pluginLayerType() ==
                TRasterLayer.LAYER_TYPE):
            self.actionMoveRaster.setEnabled(True)
            self.actionRotateRaster.setEnabled(True)
            self.actionScaleRaster.setEnabled(True)
            self.actionAdjustRaster.setEnabled(True)
            self.actionBarRescaleRaster.setEnabled(True)
            self.actionGeoref2PRaster.setEnabled(True)
            self.actionDecreaseTransparency.setEnabled(True)
            self.actionIncreaseTransparency.setEnabled(True)
            self.actionUndo.setEnabled(True)
            self.actionExport.setEnabled(True)
            self.actionPointIncrement.setEnabled(False)
            self.actionCoordFromPoints.setEnabled(False)
            self.actionCreateTransect.setEnabled(False)
            self.actionImportFile.setEnabled(False)
        else:
            self.actionMoveRaster.setEnabled(False)
            self.actionRotateRaster.setEnabled(False)
            self.actionScaleRaster.setEnabled(False)
            self.actionAdjustRaster.setEnabled(False)
            self.actionBarRescaleRaster.setEnabled(False)
            self.actionGeoref2PRaster.setEnabled(False)
            self.actionDecreaseTransparency.setEnabled(False)
            self.actionIncreaseTransparency.setEnabled(False)
            self.actionUndo.setEnabled(False)
            self.actionExport.setEnabled(False)

            #Enable/disable for point (0) layer
            self.actionPointIncrement.setEnabled(False)
            self.actionCoordFromPoints.setEnabled(False)
            self.actionCreateTransect.setEnabled(False)
            self.actionImportFile.setEnabled(False)
            if layer and layer.type() == QgsMapLayer.VectorLayer:
                if layer.geometryType() == 0:
                    self.actionPointIncrement.setEnabled(True)
                    self.actionCoordFromPoints.setEnabled(True)
                    self.actionCreateTransect.setEnabled(True)
                    self.actionImportFile.setEnabled(True)

    def addLayer(self):
        #self.dialogAddLayer.clear(self.layer)  #Need self.layer??
        self.dialogAddLayer.clear()
        self.dialogAddLayer.show()
        result = self.dialogAddLayer.exec_()
        if result == 1:
            self.createTRasterLayer()

    def createTRasterLayer(self):
        imagePath = self.dialogAddLayer.lineEditImagePath.text()
        imageName, _ = os.path.splitext(os.path.basename(imagePath))

        screenExtent = self.iface.mapCanvas().extent()

        layer = TRasterLayer(
            self, imagePath, imageName, screenExtent)
        if layer.isValid():
            QgsProject.instance().addMapLayer(layer)
            self.layers[layer.id()] = layer
            self.iface.setActiveLayer(layer)

    def moveRaster(self):
        self.currentTool = self.moveTool
        layer = self.iface.activeLayer()
        self.moveTool.setLayer(layer)
        self.iface.mapCanvas().setMapTool(self.moveTool)

    def rotateRaster(self):
        self.currentTool = self.rotateTool
        layer = self.iface.activeLayer()
        self.rotateTool.setLayer(layer)
        self.iface.mapCanvas().setMapTool(self.rotateTool)

    def scaleRaster(self):
        self.currentTool = self.scaleTool
        layer = self.iface.activeLayer()
        self.scaleTool.setLayer(layer)
        self.iface.mapCanvas().setMapTool(self.scaleTool)

    def adjustRaster(self):
        self.currentTool = self.adjustTool
        layer = self.iface.activeLayer()
        self.adjustTool.setLayer(layer)
        self.iface.mapCanvas().setMapTool(self.adjustTool)

    def barRescaleRaster(self):
        self.currentTool = self.barRescaleTool
        layer = self.iface.activeLayer()
        self.barRescaleTool.setLayer(layer)
        self.iface.mapCanvas().setMapTool(self.barRescaleTool)

    def georef2PRaster(self):
        self.currentTool = self.georef2PTool
        layer = self.iface.activeLayer()
        self.georef2PTool.setLayer(layer)
        self.iface.mapCanvas().setMapTool(self.georef2PTool)

    def increaseTransparency(self):
        layer = self.iface.activeLayer()
        # clamp to 100
        tr = min(layer.transparency + 10, 100)
        layer.transparencyChanged(tr)

    def decreaseTransparency(self):
        layer = self.iface.activeLayer()
        # clamp to 0
        tr = max(layer.transparency - 10, 0)
        layer.transparencyChanged(tr)

    def undo(self):
        layer = self.iface.activeLayer()
        if self.currentTool:
            self.currentTool.reset() # for clear 2point rubberband
            self.currentTool.setLayer(layer)
        if len(layer.history) > 0:
            act = layer.history.pop()
            if act["action"] == "move":
                layer.setCenter(act["center"])
            elif act["action"] == "scale":
                layer.setScale(act["xScale"], act["yScale"])
            elif act["action"] == "rotation":
                layer.setRotation(act["rotation"])
                layer.setCenter(act["center"])
            elif act["action"] == "adjust":
                layer.setCenter(act["center"])
                layer.setScale(act["xScale"], act["yScale"])
            elif act["action"] == "BarRescale":
                layer.setScale(act["xScale"], act["yScale"])
            elif act["action"] == "2PClickA":
                layer.setCenter(act["center"])
            elif act["action"] == "2PClickB":
                layer.setRotation(act["rotation"])
                layer.setCenter(act["center"])
                layer.setScale(act["xScale"], act["yScale"])
                layer.setScale(act["xScale"], act["yScale"])
            layer.repaint()
            layer.commitTransformParameters()

    def exportGeorefRaster(self):
        layer = self.iface.activeLayer()
        self.dialogExportGeorefRaster.clear(layer)
        self.dialogExportGeorefRaster.show()
        result = self.dialogExportGeorefRaster.exec_()
        if result == 1:
            exportCommand = ExportGeorefRasterCommand(self.iface)
            exportCommand.exportGeorefRaster(
                layer, self.dialogExportGeorefRaster.imagePath,
                self.dialogExportGeorefRaster.isPutRotationInWorldFile)

    def pointIncrement(self):
        self.currentTool = self.incrementTool
        layer = self.iface.activeLayer()
        self.incrementTool.setLayer(layer)
        self.iface.mapCanvas().setMapTool(self.incrementTool)
        self.incrementTool.initialiseIncrement()

    def coordFromPoints(self):
        self.currentTool = self.coordPointsTool
        layer = self.iface.activeLayer()
        self.coordPointsTool.setLayer(layer)
        self.iface.mapCanvas().setMapTool(self.coordPointsTool)
        self.coordPointsTool.getCoords()

    def createGrid(self):
        self.currentTool = self.createGridTool
        self.iface.mapCanvas().setMapTool(self.createGridTool)

    def createTransect(self):
        self.currentTool = self.createTransectTool
        layer = self.iface.activeLayer()
        self.createTransectTool.setLayer(layer)
        self.iface.mapCanvas().setMapTool(self.createTransectTool)

    def importFile(self):
        self.currentTool = self.importFileTool
        layer = self.iface.activeLayer()
        self.importFileTool.setLayer(layer)
        self.iface.mapCanvas().setMapTool(self.importFileTool)
        self.importFileTool.loadCSV()

    def openHelp(self):
        showPluginHelp()