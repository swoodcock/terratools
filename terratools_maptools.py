# -*- coding: utf-8 -*

import math, numpy, operator
from operator import itemgetter

from PyQt5.QtCore import Qt, QVariant
from PyQt5.QtWidgets import (QApplication, QInputDialog, QMessageBox,
                             QFileDialog)
from qgis.core import (QgsProject, QgsPointXY, QgsGeometry,
                       QgsWkbTypes, QgsFeature, QgsField,
                       QgsProcessingFeedback, QgsPalLayerSettings,
                       QgsVectorLayerSimpleLabeling)
from qgis.gui import QgsMapToolEmitPoint, QgsRubberBand
from qgis import processing

from .tempmapcanvasitem import TempMapCanvasItem
from .utils import tryfloat


def isLayerVisible(iface, layer):
    # TODO Really ???? See if there is something simpler
    vl = iface.layerTreeView().layerTreeModel().rootGroup().findLayer(layer)
    return vl.itemVisibilityChecked()


def setLayerVisible(iface, layer, visible):
    vl = iface.layerTreeView().layerTreeModel().rootGroup().findLayer(layer)
    vl.setItemVisibilityChecked(visible)

#TODO Minor Bug Fix: Hold left click then right click causes redraw issues
class MoveRasterMapTool(QgsMapToolEmitPoint):
    def __init__(self, iface):
        self.iface = iface
        self.canvas = iface.mapCanvas()
        QgsMapToolEmitPoint.__init__(self, self.canvas)

        self.rasterShadow = TempMapCanvasItem(self.canvas)

        self.rubberBandDisplacement = QgsRubberBand(
            self.canvas, QgsWkbTypes.LineGeometry)
        self.rubberBandDisplacement.setColor(Qt.red)
        self.rubberBandDisplacement.setWidth(1)

        self.rubberBandExtent = QgsRubberBand(
            self.canvas, QgsWkbTypes.LineGeometry)
        self.rubberBandExtent.setColor(Qt.red)
        self.rubberBandExtent.setWidth(1)

        self.isLayerVisible = True

        self.reset()

        self.pressed_button = None

    def setLayer(self, layer):
        self.layer = layer

    def reset(self):
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False
        self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)
        self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
        self.rasterShadow.reset()
        self.layer = None
        self.secondClick = False

    def deactivate(self):
        QgsMapToolEmitPoint.deactivate(self)
        self.reset()

    def canvasPressEvent(self, e):
        self.pressed_button = e.button()
        if self.pressed_button == 1:
            self.startPoint = self.toMapCoordinates(e.pos())
            self.endPoint = self.startPoint
            self.isEmittingPoint = True
            self.originalCenter = self.layer.center
            # this tool do the displacement itself TODO update so it is done by
            # transformed coordinates + new center)
            self.originalCornerPoints = self.layer.transformedCornerCoordinates(
                *self.layer.transformParameters())

            self.isLayerVisible = isLayerVisible(self.iface, self.layer)
            setLayerVisible(self.iface, self.layer, False)

            self.showDisplacement(self.startPoint, self.endPoint)
            self.layer.history.append({"action": "move", "center": self.layer.center})

    def canvasReleaseEvent(self, e):
        self.pressed_button = e.button()
        if self.pressed_button == 1:
            self.secondClick = False

            self.isEmittingPoint = False

            self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)
            self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
            self.rasterShadow.reset()

            x = self.originalCenter.x() + self.endPoint.x() - self.startPoint.x()
            y = self.originalCenter.y() + self.endPoint.y() - self.startPoint.y()
            self.layer.setCenter(QgsPointXY(x, y))

            setLayerVisible(self.iface, self.layer,
                            self.isLayerVisible)
            self.layer.repaint()

            self.layer.commitTransformParameters()

        if self.pressed_button == 2:
            if self.secondClick == False:
                self.startPoint = self.toMapCoordinates(e.pos())
                self.endPoint = self.startPoint
                self.originalCenter = self.layer.center
                self.originalCornerPoints = self.layer.transformedCornerCoordinates(
                    *self.layer.transformParameters())
                self.secondClick = True

            elif self.secondClick == True:
                self.rasterShadow.reset()
                self.endPoint = self.toMapCoordinates(e.pos())
                x = self.originalCenter.x() + self.endPoint.x() - self.startPoint.x()
                y = self.originalCenter.y() + self.endPoint.y() - self.startPoint.y()
                self.layer.setCenter(QgsPointXY(x, y))

                self.layer.repaint()
                self.layer.commitTransformParameters()
                self.secondClick = False

    def canvasMoveEvent(self, e):
        if self.pressed_button == 1:
            if not self.isEmittingPoint:
                return

            self.endPoint = self.toMapCoordinates(e.pos())
            self.showDisplacement(self.startPoint, self.endPoint)

    def showDisplacement(self, startPoint, endPoint):
        self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)
        point1 = QgsPointXY(startPoint.x(), startPoint.y())
        point2 = QgsPointXY(endPoint.x(), endPoint.y())
        self.rubberBandDisplacement.addPoint(point1, False)
        self.rubberBandDisplacement.addPoint(
            point2, True)  # true to update canvas
        self.rubberBandDisplacement.show()

        self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
        for point in self.originalCornerPoints:
            self._addDisplacementToPoint(self.rubberBandExtent, point, False)
        # for closing
        self._addDisplacementToPoint(
            self.rubberBandExtent, self.originalCornerPoints[0], True)
        self.rubberBandExtent.show()

        self.rasterShadow.reset(self.layer)
        self.rasterShadow.setDeltaDisplacement(self.endPoint.x(
        ) - self.startPoint.x(), self.endPoint.y() - self.startPoint.y(), True)
        self.rasterShadow.show()

    def _addDisplacementToPoint(self, rubberBand, point, doUpdate):
        x = point.x() + self.endPoint.x() - self.startPoint.x()
        y = point.y() + self.endPoint.y() - self.startPoint.y()
        self.rubberBandExtent.addPoint(QgsPointXY(x, y), doUpdate)


# move the mouse in the Y axis to rotate
class RotateRasterMapTool(QgsMapToolEmitPoint):
    def __init__(self, iface):
        self.iface = iface
        self.canvas = iface.mapCanvas()
        QgsMapToolEmitPoint.__init__(self, self.canvas)

        self.rasterShadow = TempMapCanvasItem(self.canvas)

        self.rubberBandExtent = QgsRubberBand(
            self.canvas, QgsWkbTypes.LineGeometry)
        self.rubberBandExtent.setColor(Qt.red)
        self.rubberBandExtent.setWidth(1)

        # In case of rotation around pressed point (ctrl)
        # Use rubberBand for displaying an horizontal line.
        self.rubberBandDisplacement = QgsRubberBand(
            self.canvas, QgsWkbTypes.LineGeometry)
        self.rubberBandDisplacement.setColor(Qt.red)
        self.rubberBandDisplacement.setWidth(1)

        self.reset()

    def setLayer(self, layer):
        self.layer = layer

    def reset(self):
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False
        self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
        self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)
        self.rasterShadow.reset()
        self.layer = None

    def canvasPressEvent(self, e):
        pressed_button = e.button()
        if pressed_button == 1:
            self.startY = e.pos().y()
            self.endY = self.startY
            self.isEmittingPoint = True
            self.height = self.canvas.height()

            modifiers = QApplication.keyboardModifiers()
            self.isRotationAroundPoint = bool(modifiers & Qt.ControlModifier)
            self.startPoint = self.toMapCoordinates(e.pos())
            self.endPoint = self.startPoint

            self.isLayerVisible = isLayerVisible(self.iface, self.layer)
            setLayerVisible(self.iface, self.layer, False)

            rotation = self.computeRotation()
            self.showRotation(rotation)
        self.layer.history.append({"action": "rotation", "rotation": self.layer.rotation,
                                   "center": self.layer.center})


    def canvasReleaseEvent(self, e):
        pressed_button = e.button()
        if pressed_button == 1:
            self.isEmittingPoint = False

            self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
            self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)
            self.rasterShadow.reset()

            rotation = self.computeRotation()
            if self.isRotationAroundPoint:
                self.layer.moveCenterFromPointRotate(
                    self.startPoint, rotation, 1, 1)
            self.layer.setRotation(self.layer.rotation + rotation)

            setLayerVisible(self.iface, self.layer,
                            self.isLayerVisible)

        elif pressed_button == 2:
            number, ok = QInputDialog.getText(
                None, "Rotate by value", "Enter + or - degrees:")
            if not ok:
                self.layer.history.pop()
                return
            rotateNum = tryfloat(number)
            if rotateNum:
                if rotateNum > 360 or rotateNum < -360:
                    self.layer.history.pop()
                    QMessageBox.information(
                        self.iface.mainWindow(),
                        "Error",
                        "Number can't exceed 360")
                    return
            else:
                self.layer.history.pop()
                QMessageBox.information(
                    self.iface.mainWindow(),
                    "Error",
                    "Bad format: Must be numerical")
                return

            self.layer.setRotation(self.layer.rotation + rotateNum)

        self.layer.repaint()
        self.layer.commitTransformParameters()

    def canvasMoveEvent(self, e):
        if not self.isEmittingPoint:
            return

        self.endY = e.pos().y()
        rotation = self.computeRotation()
        self.showRotation(rotation)

        self.endPoint = self.toMapCoordinates(e.pos())

    def computeRotation(self):
        if self.isRotationAroundPoint:
            dX = self.endPoint.x() - self.startPoint.x()
            dY = self.endPoint.y() - self.startPoint.y()
            return math.degrees(math.atan2(-dY, dX))
        else:
            dY = self.endY - self.startY
            return 90.0 * dY / self.height

    def showRotation(self, rotation):
        if self.isRotationAroundPoint:
            cornerPoints = self.layer.transformedCornerCoordinatesFromPoint(
                self.startPoint, rotation, 1, 1)

            self.rasterShadow.reset(self.layer)
            self.rasterShadow.setDeltaRotationFromPoint(
                rotation, self.startPoint, True)
            self.rasterShadow.show()

            self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)
            point0 = QgsPointXY(self.startPoint.x() + 10, self.startPoint.y())
            point1 = QgsPointXY(self.startPoint.x(), self.startPoint.y())
            point2 = QgsPointXY(self.endPoint.x(), self.endPoint.y())
            self.rubberBandDisplacement.addPoint(point0, False)
            self.rubberBandDisplacement.addPoint(point1, False)
            self.rubberBandDisplacement.addPoint(
                point2, True)  # true to update canvas
            self.rubberBandDisplacement.show()
        else:
            center, originalRotation, xScale, yScale = \
                self.layer.transformParameters()
            newRotation = rotation + originalRotation
            cornerPoints = self.layer.transformedCornerCoordinates(
                center, newRotation, xScale, yScale)

            self.rasterShadow.reset(self.layer)
            self.rasterShadow.setDeltaRotation(rotation, True)
            self.rasterShadow.show()

        self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
        for point in cornerPoints:
            self.rubberBandExtent.addPoint(point, False)
        # for closing
        self.rubberBandExtent.addPoint(cornerPoints[0], True)
        self.rubberBandExtent.show()


# move the map in x or y axis to scale in x or y dimensions of the
# image (no rotation of the coordinate system)
class ScaleRasterMapTool(QgsMapToolEmitPoint):
    def __init__(self, iface):
        self.iface = iface
        self.canvas = iface.mapCanvas()
        QgsMapToolEmitPoint.__init__(self, self.canvas)

        self.rasterShadow = TempMapCanvasItem(self.canvas)

        self.rubberBandExtent = QgsRubberBand(
            self.canvas, QgsWkbTypes.LineGeometry)
        self.rubberBandExtent.setColor(Qt.red)
        self.rubberBandExtent.setWidth(1)

        self.reset()

    def setLayer(self, layer):
        self.layer = layer

    def reset(self):
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False
        self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
        self.rasterShadow.reset()
        self.layer = None

    def canvasPressEvent(self, e):
        #e = event, for button 1=left click, 2=right click
        pressed_button = e.button()
        if pressed_button == 1:
            self.startPoint = e.pos()
            self.endPoint = self.startPoint
            self.isEmittingPoint = True
            self.height = float(self.canvas.height())
            self.width = float(self.canvas.width())

            modifiers = QApplication.keyboardModifiers()
            self.isKeepRelativeScale = bool(modifiers & Qt.ControlModifier)

            self.isLayerVisible = isLayerVisible(self.iface,
                                                 self.layer)
            setLayerVisible(self.iface, self.layer, False)

            scaling = self.computeScaling()
            self.showScaling(*scaling)
        self.layer.history.append({"action": "scale", "xScale": self.layer.xScale, "yScale": self.layer.yScale})

    def canvasReleaseEvent(self, e):
        pressed_button = e.button()
        if pressed_button == 1:
            self.isEmittingPoint = False

            self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
            self.rasterShadow.reset()

            xScale, yScale = self.computeScaling()
            self.layer.setScale(xScale * self.layer.xScale,
                                yScale * self.layer.yScale)

            setLayerVisible(self.iface, self.layer, self.isLayerVisible)
        elif pressed_button == 2:
            number, ok = QInputDialog.getText(
                None, "Scale & DPI", """Enter comma separated values:

   1) Scale (1:?)
   2) DPI (pixels)
   
e.g. 3000,96""")
            if not ok:
                self.layer.history.pop()
                return
            scales = number.split(',')
            if len(scales) != 2:
                self.layer.history.pop()
                QMessageBox.information(
                    self.iface.mainWindow(),
                    "Error",
                    "Must be 2 numbers")
                return
            scale = tryfloat(scales[0])
            dpi = tryfloat(scales[1])
            if scale and dpi:
                xScale = scale / (dpi / 0.0254)
                yScale = xScale
            else:
                self.layer.history.pop()
                QMessageBox.information(
                    self.iface.mainWindow(),
                    "Error",
                    "Bad format: Must be scale,dpi (e.g. 3000,96)")
                return

            self.layer.setScale(xScale, yScale)

        self.layer.repaint()
        self.layer.commitTransformParameters()

    def canvasMoveEvent(self, e):
        if not self.isEmittingPoint:
            return

        self.endPoint = e.pos()
        scaling = self.computeScaling()
        self.showScaling(*scaling)

    def computeScaling(self):
        dX = -(self.endPoint.x() - self.startPoint.x())
        dY = self.endPoint.y() - self.startPoint.y()
        xScale = 1.0 - (dX / (self.width * 1.1))
        yScale = 1.0 - (dY / (self.height * 1.1))

        if self.isKeepRelativeScale:
            # keep same scale in both dimensions
            return (xScale, xScale)
        else:
            return (xScale, yScale)

    def showScaling(self, xScale, yScale):
        if xScale == 0 and yScale == 0:
            return

        center, rotation, originalXScale, originalYScale = \
            self.layer.transformParameters()
        newXScale = xScale * originalXScale
        newYScale = yScale * originalYScale
        cornerPoints = self.layer.transformedCornerCoordinates(
            center, rotation, newXScale, newYScale)

        self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
        for point in cornerPoints:
            self.rubberBandExtent.addPoint(point, False)
        # for closing
        self.rubberBandExtent.addPoint(cornerPoints[0], True)
        self.rubberBandExtent.show()

        self.rasterShadow.reset(self.layer)
        self.rasterShadow.setDeltaScale(xScale, yScale, True)
        self.rasterShadow.show()


class AdjustRasterMapTool(QgsMapToolEmitPoint):
    def __init__(self, iface):
        self.iface = iface
        self.canvas = iface.mapCanvas()
        QgsMapToolEmitPoint.__init__(self, self.canvas)

        self.rasterShadow = TempMapCanvasItem(self.canvas)

        self.rubberBandExtent = QgsRubberBand(
            self.canvas, QgsWkbTypes.LineGeometry)
        self.rubberBandExtent.setColor(Qt.red)
        self.rubberBandExtent.setWidth(1)

        self.rubberBandAdjustSide = QgsRubberBand(
            self.canvas, QgsWkbTypes.LineGeometry)
        self.rubberBandAdjustSide.setColor(Qt.red)
        self.rubberBandAdjustSide.setWidth(3)

        self.reset()

    def setLayer(self, layer):
        self.layer = layer

    def reset(self):
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False
        self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
        self.rubberBandAdjustSide.reset(QgsWkbTypes.LineGeometry)
        self.rasterShadow.reset()
        self.layer = None

    def canvasPressEvent(self, e):
        # find the side of the rectangle closest to the click and some data
        # necessary to compute the new cneter and scale
        topLeft, topRight, bottomRight, bottomLeft = \
            self.layer.cornerCoordinates()
        top = [topLeft, topRight]
        right = [bottomRight, topRight]
        bottom = [bottomRight, bottomLeft]
        left = [bottomLeft, topLeft]

        click = QgsGeometry.fromPointXY(self.toMapCoordinates(e.pos()))

        # order is important (for referenceSide)
        sides = [top, right, bottom, left]
        distances = [click.distance(
            QgsGeometry.fromPolylineXY(side)) for side in sides]
        self.indexSide = self.minDistance(distances)
        self.side = sides[self.indexSide]
        self.sidePoint = self.center(self.side)
        self.vector = self.directionVector(self.side)
        # side that does not move (opposite of indexSide)
        self.referenceSide = sides[(self.indexSide + 2) % 4]
        self.referencePoint = self.center(self.referenceSide)
        self.referenceDistance = self.distance(
            self.sidePoint, self.referencePoint)
        self.isXScale = self.indexSide % 2 == 1

        self.startPoint = click.asPoint()
        self.endPoint = self.startPoint
        self.isEmittingPoint = True

        self.isLayerVisible = isLayerVisible(self.iface,
                                             self.layer)
        setLayerVisible(self.iface, self.layer, False)

        adjustment = self.computeAdjustment()
        self.showAdjustment(*adjustment)
        self.layer.history.append(
            {"action": "adjust", "center": self.layer.center, "xScale":
                self.layer.xScale, "yScale": self.layer.yScale})

    def minDistance(self, distances):
        sortedDistances = [i[0] for i in sorted(
            enumerate(distances), key=itemgetter(1))]
        # first is min
        return sortedDistances[0]

    def directionVector(self, side):
        sideCenter = self.center(side)
        layerCenter = self.layer.center
        vector = [sideCenter.x() - layerCenter.x(),
                  sideCenter.y() - layerCenter.y()]
        norm = math.sqrt(vector[0]**2 + vector[1]**2)
        normedVector = [vector[0] / norm, vector[1] / norm]
        return normedVector

    def center(self, side):
        return QgsPointXY((side[0].x() + side[1].x()) / 2,
                          (side[0].y() + side[1].y()) / 2)

    def distance(self, pt1, pt2):
        return math.sqrt((pt1.x() - pt2.x())**2 + (pt1.y() - pt2.y())**2)

    def canvasReleaseEvent(self, e):
        self.isEmittingPoint = False

        self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
        self.rubberBandAdjustSide.reset(QgsWkbTypes.LineGeometry)
        self.rasterShadow.reset()

        center, xScale, yScale = self.computeAdjustment()
        self.layer.setCenter(center)
        self.layer.setScale(xScale * self.layer.xScale,
                            yScale * self.layer.yScale)

        setLayerVisible(self.iface, self.layer,
                        self.isLayerVisible)
        self.layer.repaint()

        self.layer.commitTransformParameters()

    def canvasMoveEvent(self, e):
        if not self.isEmittingPoint:
            return

        self.endPoint = self.toMapCoordinates(e.pos())

        adjustment = self.computeAdjustment()
        self.showAdjustment(*adjustment)

    def computeAdjustment(self):
        dX = self.endPoint.x() - self.startPoint.x()
        dY = self.endPoint.y() - self.startPoint.y()
        # project on vector
        dp = dX * self.vector[0] + dY * self.vector[1]

        # do not go beyond 5% of the current size of side
        if dp < -0.95 * self.referenceDistance:
            dp = -0.95 * self.referenceDistance

        updatedSidePoint = QgsPointXY(self.sidePoint.x() + dp * self.vector[0],
                                      self.sidePoint.y() + dp * self.vector[1])

        center = self.center([self.referencePoint, updatedSidePoint])
        scaleFactor = self.distance(self.referencePoint, updatedSidePoint)
        if self.isXScale:
            xScale = scaleFactor / self.referenceDistance
            yScale = 1.0
        else:
            xScale = 1.0
            yScale = scaleFactor / self.referenceDistance

        return (center, xScale, yScale)

    def showAdjustment(self, center, xScale, yScale):
        _, rotation, originalXScale, originalYScale = \
            self.layer.transformParameters()
        newXScale = xScale * originalXScale
        newYScale = yScale * originalYScale
        cornerPoints = self.layer.transformedCornerCoordinates(
            center, rotation, newXScale, newYScale)

        self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
        for point in cornerPoints:
            self.rubberBandExtent.addPoint(point, False)
        # for closing
        self.rubberBandExtent.addPoint(cornerPoints[0], True)
        self.rubberBandExtent.show()

        # show rubberband for side
        # see def of indexSide in init:
        # cornerpoints are (topLeft, topRight, bottomRight, bottomLeft)
        self.rubberBandAdjustSide.reset(QgsWkbTypes.LineGeometry)
        self.rubberBandAdjustSide.addPoint(
            cornerPoints[self.indexSide % 4], False)
        self.rubberBandAdjustSide.addPoint(
            cornerPoints[(self.indexSide + 1) % 4], True)
        self.rubberBandAdjustSide.show()

        self.rasterShadow.reset(self.layer)
        dx = center.x() - self.layer.center.x()
        dy = center.y() - self.layer.center.y()
        self.rasterShadow.setDeltaDisplacement(dx, dy, False)
        self.rasterShadow.setDeltaScale(xScale, yScale, True)
        self.rasterShadow.show()


class BarRescaleRasterMapTool(QgsMapToolEmitPoint):
    def __init__(self, iface):
        self.iface = iface
        self.canvas = iface.mapCanvas()
        QgsMapToolEmitPoint.__init__(self, self.canvas)

        self.rasterShadow = TempMapCanvasItem(self.canvas) #DELETE?

        self.rubberBandDisplacement = QgsRubberBand(
            self.canvas, QgsWkbTypes.LineGeometry)
        self.rubberBandDisplacement.setColor(Qt.red)
        self.rubberBandDisplacement.setWidth(1)

        self.isLayerVisible = True
        self.reset()

    def setLayer(self, layer):
        self.layer = layer

    def reset(self):
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False
        self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)
        self.rasterShadow.reset() #DELETE?
        self.layer = None

    def deactivate(self):
        QgsMapToolEmitPoint.deactivate(self)
        self.reset()

    def canvasPressEvent(self, e):
        self.pressed_button = e.button()
        if self.pressed_button == 2:
            return
        self.startPoint = self.toMapCoordinates(e.pos())
        self.endPoint = self.startPoint
        self.isEmittingPoint = True
        #self.originalCenter = self.layer.center
        self.layer.history.append({"action": "BarRescale", "xScale": self.layer.xScale, "yScale": self.layer.yScale})

    def canvasReleaseEvent(self, e):
        self.pressed_button = e.button()
        if self.pressed_button == 2:
            return
        self.isEmittingPoint = False
        self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)
        # x = (self.originalCenter.x() + self.endPoint.x() -
        #      self.startPoint.x())
        # y = (self.originalCenter.y() + self.endPoint.y() -
        #      self.startPoint.y())
    #         self.rasterShadow.reset()
        clickDist = math.sqrt((self.endPoint.x() - self.startPoint.x()
                               ) ** 2 + (self.endPoint.y() - self.startPoint.y()) ** 2)

        number, ok = QInputDialog.getText(
            None, "Set length", "Equivalent distance clicked (m):")
        if not ok:
            self.layer.history.pop()
            return
        lenScaleBar = tryfloat(number)
        if lenScaleBar and clickDist:
            # 0.0254 inches per meter, use for dpi calculations
            # xScale = scale / (dpi / 0.0254)
            scaleFactor = self.layer.xScale * (lenScaleBar / clickDist)
        else:
            self.layer.history.pop()
            QMessageBox.information(
                self.iface.mainWindow(),
                "Error",
                "Must be a single number")
            return

        # Place this within if code above? (under scalefactor = ):
        self.layer.setScale(scaleFactor, scaleFactor)

        self.layer.repaint()
        self.layer.commitTransformParameters()

    def canvasMoveEvent(self, e):
        if not self.isEmittingPoint:
            return

        self.endPoint = self.toMapCoordinates(e.pos())
        self.showDisplacement(self.startPoint, self.endPoint)

    def showDisplacement(self, startPoint, endPoint):
        self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)
        point1 = QgsPointXY(startPoint.x(), startPoint.y())
        point2 = QgsPointXY(endPoint.x(), endPoint.y())
        self.rubberBandDisplacement.addPoint(point1, False)
        self.rubberBandDisplacement.addPoint(
            point2, True)  # true to update canvas
        self.rubberBandDisplacement.show()


#             number, ok = QInputDialog.getText(
#                 None, "Length, Scale & DPI", """Enter comma separated values:
#
#   1) Clicked length (m)
#   2) Scale (m, 1:?)
#   3) DPI (pixels, default 300)
#
# e.g. 1000,10000,300""")
#             if not ok:
#                 return
#             scales = number.split(',')
#             if len(scales) != 3:
#                 QMessageBox.information(
#                     self.iface.mainWindow(),
#                     "Error",
#                     "Must be 3 numbers")
#                 return
#             lenScaleBar = tryfloat(scales[0])
#             mapScaleRatio = tryfloat(scales[1])
#             dpi = tryfloat(scales[2])
#             if lenScaleBar and mapScaleRatio and dpi:
#                 # 0.0254 inches per meter, use for dpi calculations
#                 # xScale = scale / (dpi / 0.0254)
#                 scaleFactor = self.layer.xScale * (lenScaleBar / clickDist)
#
#             else:
#                 QMessageBox.information(
#                     self.iface.mainWindow(),
#                     "Error",
#                     "Bad format: Must be length,scale,dpi (e.g. 1000,10000,300)")
#                 return


class GeorefRasterBy2PointsMapTool(QgsMapToolEmitPoint):
    def __init__(self, iface):
        self.iface = iface
        self.canvas = iface.mapCanvas()
        QgsMapToolEmitPoint.__init__(self, self.canvas)

        self.rasterShadow = TempMapCanvasItem(self.canvas)

        self.firstPoint = None

        self.rubberBandOrigin = QgsRubberBand(
            self.canvas, QgsWkbTypes.PointGeometry)
        self.rubberBandOrigin.setColor(Qt.red)
        self.rubberBandOrigin.setIcon(QgsRubberBand.ICON_CIRCLE)
        self.rubberBandOrigin.setIconSize(7)
        self.rubberBandOrigin.setWidth(2)

        self.rubberBandDisplacement = QgsRubberBand(
            self.canvas, QgsWkbTypes.LineGeometry)
        self.rubberBandDisplacement.setColor(Qt.red)
        self.rubberBandDisplacement.setWidth(1)

        self.rubberBandExtent = QgsRubberBand(
            self.canvas, QgsWkbTypes.LineGeometry)
        self.rubberBandExtent.setColor(Qt.red)
        self.rubberBandExtent.setWidth(2)

        self.isLayerVisible = True

        self.reset()

    def setLayer(self, layer):
        self.layer = layer

    def reset(self):
        self.startPoint = self.endPoint = self.firstPoint = None
        self.isEmittingPoint = False
        self.rubberBandOrigin.reset(QgsWkbTypes.PointGeometry)
        self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)
        self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
        self.rasterShadow.reset()
        self.layer = None

    def deactivate(self):
        QgsMapToolEmitPoint.deactivate(self)
        self.reset()

    def canvasPressEvent(self, e):
        if self.firstPoint is None:
            self.startPoint = self.toMapCoordinates(e.pos())
            self.endPoint = self.startPoint
            self.isEmittingPoint = True
            self.originalCenter = self.layer.center
            # this tool do the displacement itself TODO update so it is done by
            # transformed coordinates + new center)
            self.originalCornerPoints = \
                self.layer.transformedCornerCoordinates(
                    *self.layer.transformParameters())

            self.isLayerVisible = isLayerVisible(self.iface,
                                                 self.layer)
            setLayerVisible(self.iface, self.layer, False)

            self.showDisplacement(self.startPoint, self.endPoint)
            self.layer.history.append({"action": "2PClickA", "center": self.layer.center})
        else:
            self.startPoint = self.toMapCoordinates(e.pos())
            self.endPoint = self.startPoint

            self.startY = e.pos().y()
            self.endY = self.startY
            self.isEmittingPoint = True
            self.height = self.canvas.height()

            self.isLayerVisible = isLayerVisible(self.iface,
                                                 self.layer)
            setLayerVisible(self.iface, self.layer, False)

            rotation = self.computeRotation()
            xScale = yScale = self.computeScale()
            self.showRotationScale(rotation, xScale, yScale)
            self.layer.history.append(
                {"action": "2PClickB", "center": self.layer.center, "xScale": self.layer.xScale,
                 "yScale": self.layer.yScale, "rotation": self.layer.rotation})

    def canvasReleaseEvent(self, e):
        self.isEmittingPoint = False

        self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)
        self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
        self.rasterShadow.reset()

        if self.firstPoint is None:
            x = (self.originalCenter.x() + self.endPoint.x() -
                 self.startPoint.x())
            y = (self.originalCenter.y() + self.endPoint.y() -
                 self.startPoint.y())
            self.layer.setCenter(QgsPointXY(x, y))
            self.firstPoint = self.endPoint

            setLayerVisible(self.iface, self.layer,
                            self.isLayerVisible)
            self.layer.repaint()

            self.layer.commitTransformParameters()
        else:
            rotation = self.computeRotation()
            xScale = yScale = self.computeScale()
            self.layer.moveCenterFromPointRotate(
                self.firstPoint, rotation, xScale, yScale)
            self.layer.setRotation(self.layer.rotation + rotation)
            self.layer.setScale(self.layer.xScale * xScale,
                                self.layer.yScale * yScale)

            setLayerVisible(self.iface, self.layer,
                            self.isLayerVisible)
            self.layer.repaint()

            self.layer.commitTransformParameters()

            self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)
            self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
            self.rubberBandOrigin.reset(QgsWkbTypes.PointGeometry)
            self.rasterShadow.reset()

            self.firstPoint = None
            self.startPoint = self.endPoint = None

    def canvasMoveEvent(self, e):
        if not self.isEmittingPoint:
            return

        self.endPoint = self.toMapCoordinates(e.pos())

        if self.firstPoint is None:
            self.showDisplacement(self.startPoint, self.endPoint)
        else:
            self.endY = e.pos().y()
            rotation = self.computeRotation()
            xScale = yScale = self.computeScale()
            self.showRotationScale(rotation, xScale, yScale)

    def computeRotation(self):
        # The angle is the difference between angle
        # horizontal/endPoint-firstPoint and horizontal/startPoint-firstPoint.
        dX0 = self.startPoint.x() - self.firstPoint.x()
        dY0 = self.startPoint.y() - self.firstPoint.y()
        dX = self.endPoint.x() - self.firstPoint.x()
        dY = self.endPoint.y() - self.firstPoint.y()
        return math.degrees(math.atan2(-dY, dX) - math.atan2(-dY0, dX0))

    def computeScale(self):
        # The scale is the ratio between endPoint-firstPoint and
        # startPoint-firstPoint.
        dX0 = self.startPoint.x() - self.firstPoint.x()
        dY0 = self.startPoint.y() - self.firstPoint.y()
        dX = self.endPoint.x() - self.firstPoint.x()
        dY = self.endPoint.y() - self.firstPoint.y()
        return math.sqrt((dX * dX + dY * dY) / (dX0 * dX0 + dY0 * dY0))

    def showRotationScale(self, rotation, xScale, yScale):
        center, _, _, _ = self.layer.transformParameters()
        # newRotation = rotation + originalRotation
        cornerPoints = self.layer.transformedCornerCoordinatesFromPoint(
            self.firstPoint, rotation, xScale, yScale)

        self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
        for point in cornerPoints:
            self.rubberBandExtent.addPoint(point, False)
        self.rubberBandExtent.addPoint(cornerPoints[0], True)
        self.rubberBandExtent.show()

        # Calculate the displacement of the center due to the rotation from
        # another point.
        newCenterDX = (cornerPoints[0].x() +
                       cornerPoints[2].x()) / 2 - center.x()
        newCenterDY = (cornerPoints[0].y() +
                       cornerPoints[2].y()) / 2 - center.y()
        self.rasterShadow.reset(self.layer)
        self.rasterShadow.setDeltaDisplacement(newCenterDX, newCenterDY, False)
        self.rasterShadow.setDeltaScale(xScale, yScale, False)
        self.rasterShadow.setDeltaRotation(rotation, True)
        self.rasterShadow.show()

        self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)
        point0 = QgsPointXY(self.startPoint.x(), self.startPoint.y())
        point1 = QgsPointXY(self.firstPoint.x(), self.firstPoint.y())
        point2 = QgsPointXY(self.endPoint.x(), self.endPoint.y())
        self.rubberBandDisplacement.addPoint(point0, False)
        self.rubberBandDisplacement.addPoint(point1, False)
        self.rubberBandDisplacement.addPoint(
            point2, True)  # true to update canvas
        self.rubberBandDisplacement.show()

    def showDisplacement(self, startPoint, endPoint):
        self.rubberBandOrigin.reset(QgsWkbTypes.PointGeometry)
        self.rubberBandOrigin.addPoint(endPoint, True)
        self.rubberBandOrigin.show()

        self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)
        point1 = QgsPointXY(startPoint.x(), startPoint.y())
        point2 = QgsPointXY(endPoint.x(), endPoint.y())
        self.rubberBandDisplacement.addPoint(point1, False)
        self.rubberBandDisplacement.addPoint(
            point2, True)  # true to update canvas
        self.rubberBandDisplacement.show()

        self.rubberBandExtent.reset(QgsWkbTypes.LineGeometry)
        for point in self.originalCornerPoints:
            self._addDisplacementToPoint(self.rubberBandExtent, point, False)
        # for closing
        self._addDisplacementToPoint(
            self.rubberBandExtent, self.originalCornerPoints[0], True)
        self.rubberBandExtent.show()

        self.rasterShadow.reset(self.layer)
        self.rasterShadow.setDeltaDisplacement(self.endPoint.x(
        ) - self.startPoint.x(), self.endPoint.y() - self.startPoint.y(), True)
        self.rasterShadow.show()

    def _addDisplacementToPoint(self, rubberBand, point, doUpdate):
        x = point.x() + self.endPoint.x() - self.startPoint.x()
        y = point.y() + self.endPoint.y() - self.startPoint.y()
        self.rubberBandExtent.addPoint(QgsPointXY(x, y), doUpdate)


class PointIncrementTool(QgsMapToolEmitPoint):
    def __init__(self, iface):
        self.iface = iface
        self.canvas = iface.mapCanvas()
        QgsMapToolEmitPoint.__init__(self, self.canvas)
        self.reset()

    def setLayer(self, layer):
        self.layer = layer

    def reset(self):
        self.layer = None

    def unsetButton(self):
        self.deactivate()
        self.canvas.unsetMapTool(self)
        self.reset()

    def canvasReleaseEvent(self, e):
        clickedPoint = self.toMapCoordinates(e.pos())
        newPoint = QgsFeature()
        newPoint.setGeometry(QgsGeometry.fromPointXY(clickedPoint))

        self.id_tail += 1
        temp_conv = str(self.id_tail).zfill(self.id_tail_len)
        full_id = self.id_head + temp_conv

        attrs = [None] * len(self.layer.fields())
        attrs[self.layer.fields().lookupField(self.selectedField)] = full_id
        newPoint.setAttributes(attrs)

        # Add point to undo/redo stack
        self.layer.beginEditCommand("Point " + full_id)
        #TODO Validation check to see if point already exists??
        self.layer.addFeatures([newPoint])
        self.layer.endEditCommand()

        self.layer.updateExtents()
        self.layer.reload()
        self.canvas.refresh()

    def initialiseIncrement(self):
        allFields = self.layer.fields()
        self.selectedField, ok = QInputDialog.getItem(
            None, "Select Field", "Select the field containing the sample ID:",
            allFields.names(), 0, False)
        if not ok:
            self.unsetButton()
            return
        #print(self.selectedField, ok, allFields[allFields.lookupField(self.selectedField)].typeName())
        if allFields[allFields.lookupField(self.selectedField)].type() == QVariant.String:
            value, ok = QInputDialog.getText(
                None, "Sample ID Mask", """Enter starting SampleID
    e.g. SSRC1""")
            if not ok:
                self.unsetButton()
                return
            if value:
                self.id_head = value.rstrip('0123456789')
                self.id_tail = value[len(self.id_head):]
                self.id_tail_len = len(self.id_tail)
                #self.idZeros = len(self.idTail)-len(self.idTail.lstrip('0'))
                if self.id_tail == "":
                    QMessageBox.information(
                        self.iface.mainWindow(),
                        "Error",
                        """Please enter ID prefix, followed by starting number.
    E.g. RCD21""")
                    self.unsetButton()
                    return
            else:
                QMessageBox.information(
                    self.iface.mainWindow(),
                    "Error",
                    "Please try again")
                self.unsetButton()
                return
        else:
            QMessageBox.information(
                self.iface.mainWindow(),
                "Error",
                "A text field must be selected.")
            self.unsetButton()
            return
        self.layer.startEditing()
        self.layer.editingStopped.connect(self.unsetButton)
        self.id_tail = int(self.id_tail) - 1


class CoordFromPointsTool(QgsMapToolEmitPoint):
    def __init__(self, iface):
        self.iface = iface
        self.canvas = iface.mapCanvas()
        QgsMapToolEmitPoint.__init__(self, self.canvas)
        self.reset()

    def setLayer(self, layer):
        self.layer = layer

    def reset(self):
        self.layer = None

    def getCoords(self):
        response = QMessageBox.question(None, "Are you sure?", "This may take time to process for large datasets.")
        if response == QMessageBox.No:
            return

        #Note: dataProvider uses updateFields, while QgsVectorLayer edits requires startEditing & commitChanges
        # layer.dataProvider().addAttributes([QgsField('xcoord', QVariant.Double),
        #                                     QgsField('ycoord', QVariant.Double)])
        # layer.updateFields()

        self.layer.startEditing()
        self.layer.addAttribute(QgsField('x_coord', QVariant.Double))
        self.layer.addAttribute(QgsField('y_coord', QVariant.Double))
        xID = self.layer.fields().indexFromName("x_coord")
        yID = self.layer.fields().indexFromName("y_coord")
        for f in self.layer.getFeatures():
            geom = f.geometry()
            x = geom.asPoint().x()
            self.layer.changeAttributeValue(f.id(), xID, x)
            y = geom.asPoint().y()
            self.layer.changeAttributeValue(f.id(), yID, y)
        # For rounding: round(y,2)
        # To save automatically: layer.commitChanges()
        self.iface.messageBar().pushSuccess(u'Success', u'Coordinates extracted into'
                                                        u' x_coord & y_coord fields.')


class CreateGridTool(QgsMapToolEmitPoint):
    def __init__(self, iface):
        self.iface = iface
        self.canvas = iface.mapCanvas()
        QgsMapToolEmitPoint.__init__(self, self.canvas)

        self.rubberBandDisplacement = QgsRubberBand(
            self.canvas, QgsWkbTypes.PolygonGeometry)
        self.rubberBandDisplacement.setStrokeColor(Qt.red)
        self.rubberBandDisplacement.setWidth(1)

        self.isLayerVisible = True
        self.reset()

    def setLayer(self, layer):
        self.layer = None

    def reset(self):
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False
        self.rubberBandDisplacement.reset(QgsWkbTypes.PolygonGeometry)
        self.layer = None

    def deactivate(self):
        QgsMapToolEmitPoint.deactivate(self)
        self.reset()

    def canvasPressEvent(self, e):
        self.pressed_button = e.button()
        if self.pressed_button == 2:
            return
        self.startPoint = self.toMapCoordinates(e.pos())
        self.endPoint = self.startPoint
        self.isEmittingPoint = True

    def canvasReleaseEvent(self, e):
        self.pressed_button = e.button()
        if self.pressed_button == 2:
            return
        self.isEmittingPoint = False
        self.rubberBandDisplacement.reset(QgsWkbTypes.PolygonGeometry)

        number, ok = QInputDialog.getText(
            None, "Create Grid", """Enter the origin local grid coordinate and desired 
grid interval (m), separated by commas:
            
e.g. 1000E,1500N,500""")
        if not ok:
            return
        values = number.split(',')
        if len(values) != 3:
            QMessageBox.information(
                self.iface.mainWindow(),
                "Error",
                "Must be 3 values")
            return
        x_local = values[0].strip()
        y_local = values[1].strip()
        x_label = x_local.strip('0123456789')
        y_label = y_local.strip('0123456789')
        x_local = tryfloat(x_local.replace(x_label,""))
        y_local = tryfloat(y_local.replace(y_label,""))
        interval = tryfloat(values[2])
        if x_local is not None and y_local is not None and interval:
            pass
        else:
            QMessageBox.information(
                self.iface.mainWindow(),
                "Error",
                """Bad format:
                
- Must be local_x,local_y,interval

(e.g. 200E,1100N, 100)""")
            return

        # Ceiling round endPoint to form whole number of grid intervals
        x_lines = math.ceil((self.endPoint.x() - self.startPoint.x()) / interval)
        y_lines = math.ceil((self.endPoint.y() - self.startPoint.y()) / interval)
        self.endPoint.set(self.startPoint.x() + (interval * x_lines),
                          self.startPoint.y() + (interval*y_lines))
        # Extent format xmin,xmax,ymin,ymax
        extent = ','.join([str(k) for k in [self.startPoint.x(),self.endPoint.x(),
                                            self.startPoint.y(),self.endPoint.y()]])
        project_crs = QgsProject.instance().crs()
        params = {
            'TYPE': 1,
            'EXTENT': extent,
            'HSPACING': interval,
            'VSPACING': interval,
            'CRS': project_crs,
            'OUTPUT': 'memory:TempGrid'
        }
        feedback = QgsProcessingFeedback()
        try:
            result = processing.run('qgis:creategrid', params, feedback=feedback)
            temp_grid = result['OUTPUT']
        except: return

        # Generate Labels
        attr_dict = {f.attributes()[0]: [f.attributes()[1], f.attributes()[2],
                                         f.attributes()[3], f.attributes()[4]]
                     for f in temp_grid.getFeatures()}

        x_min = min([attr_dict[n][0] for n in attr_dict.keys()])
        x_max = max([attr_dict[n][2] for n in attr_dict.keys()])
        y_min = min([attr_dict[n][3] for n in attr_dict.keys()])
        y_max = max([attr_dict[n][1] for n in attr_dict.keys()])
        id_hori = [k for k in attr_dict.keys() if attr_dict[k][0] ==
                   x_min and attr_dict[k][2] == x_max]
        id_vert = [k for k in attr_dict.keys() if attr_dict[k][3] ==
                   y_min and attr_dict[k][1] == y_max]

        # ALT: Linspace label generation (vert_labels = numpy.linspace
        # (x_local, x_local+((len(id_vert)-1)*interval), len(id_vert)))
        # Generate labels for different draw directions from origin
        if self.startPoint.x() < self.endPoint.x() and \
                self.startPoint.y() < self.endPoint.y():
            attr_dict = self.labelFromOrigin(attr_dict, id_vert, x_local, x_label,
                                             operator.add, reversed(id_hori), y_local,
                                             y_label, operator.add, interval)
        elif self.startPoint.x() > self.endPoint.x() and \
                self.startPoint.y() < self.endPoint.y():
            attr_dict = self.labelFromOrigin(attr_dict, reversed(id_vert), x_local,
                                             x_label, operator.sub, reversed(id_hori),
                                             y_local, y_label, operator.add, interval)
        elif self.startPoint.x() < self.endPoint.x() and \
                self.startPoint.y() > self.endPoint.y():
            attr_dict = self.labelFromOrigin(attr_dict, id_vert, x_local, x_label,
                                             operator.add, id_hori, y_local, y_label,
                                             operator.sub, interval)
        elif self.startPoint.x() > self.endPoint.x() and \
                self.startPoint.y() > self.endPoint.y():
            attr_dict = self.labelFromOrigin(attr_dict, reversed(id_vert), x_local,
                                             x_label, operator.sub, id_hori, y_local,
                                             y_label, operator.sub, interval)

        # Append to attribute table
        temp_grid.startEditing()
        temp_grid.addAttribute(QgsField('l_label', QVariant.String))
        for f in temp_grid.getFeatures():
            if len(attr_dict[f.id()]) > 4:
                label = attr_dict[f.id()][4]
                temp_grid.changeAttributeValue(f.id(), 5, label)
        temp_grid.commitChanges()

        # Apply Labels to Grid
        pal_layer = QgsPalLayerSettings()
        pal_layer.placement = QgsPalLayerSettings.Line
        pal_layer.isExpression = True
        # Display every other label, using modulo (%) function & replace letter
        pal_layer.fieldName = f"CASE WHEN regexp_replace(l_label,'[^0-9^.]','') " \
                              f"% {interval*2} = 0 THEN l_label END"
        pal_layer.enabled = True
        labels = QgsVectorLayerSimpleLabeling(pal_layer)
        temp_grid.setLabeling(labels)
        temp_grid.setLabelsEnabled(True)
        # Load Grid
        QgsProject.instance().addMapLayer(temp_grid)

    def canvasMoveEvent(self, e):
        if not self.isEmittingPoint:
            return

        self.endPoint = self.toMapCoordinates(e.pos())
        self.showDisplacement(self.startPoint, self.endPoint)

    def showDisplacement(self, startPoint, endPoint):
        self.rubberBandDisplacement.reset(QgsWkbTypes.PolygonGeometry)
        ll_extent = QgsPointXY(startPoint.x(), startPoint.y())
        ul_extent = QgsPointXY(startPoint.x(), endPoint.y())
        ur_extent = QgsPointXY(endPoint.x(), endPoint.y())
        lr_extent = QgsPointXY(endPoint.x(), startPoint.y())
        self.rubberBandDisplacement.addPoint(ll_extent, False)
        self.rubberBandDisplacement.addPoint(
            ul_extent, True)  # true to update canvas
        self.rubberBandDisplacement.addPoint(ur_extent, True)
        self.rubberBandDisplacement.addPoint(lr_extent, True)
        self.rubberBandDisplacement.show()

    def labelFromOrigin(self, attributes, vert, xnum, xlab, x_operator,
                        hori, ynum, ylab, y_operator, grid_int):
        for id in vert:
            attributes[id].append(str(int(xnum)) + xlab)
            xnum = x_operator(xnum, grid_int)
        for id in hori:
            attributes[id].append(str(int(ynum)) + ylab)
            ynum = y_operator(ynum, grid_int)
        return attributes


class CreateTransectTool(QgsMapToolEmitPoint):
    def __init__(self, iface):
        self.iface = iface
        self.canvas = iface.mapCanvas()
        QgsMapToolEmitPoint.__init__(self, self.canvas)

        self.rubberBandDisplacement = QgsRubberBand(
            self.canvas, QgsWkbTypes.LineGeometry)
        self.rubberBandDisplacement.setStrokeColor(Qt.red)
        self.rubberBandDisplacement.setWidth(1)

        self.isLayerVisible = True
        self.reset()

    def setLayer(self, layer):
        self.layer = layer

    def reset(self):
        self.startPoint = self.endPoint = None
        self.isEmittingPoint = False
        self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)
        self.layer = None

    def deactivate(self):
        QgsMapToolEmitPoint.deactivate(self)

    def unsetButton(self):
        self.deactivate()
        self.canvas.unsetMapTool(self)
        self.reset()

    def canvasPressEvent(self, e):
        self.pressed_button = e.button()
        if self.pressed_button == 2:
            return
        self.startPoint = self.toMapCoordinates(e.pos())
        self.endPoint = self.startPoint
        self.isEmittingPoint = True

    def canvasReleaseEvent(self, e):
        self.pressed_button = e.button()
        if self.pressed_button == 2:
            return
        self.isEmittingPoint = False
        self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)

        number, ok = QInputDialog.getText(
            None, "Regular Interval Points", "Specify number of points:")
        if not ok:
            return
        num_points = tryfloat(number.strip())
        if num_points:
            num_points = int(num_points)
            pass
        else:
            QMessageBox.information(
                self.iface.mainWindow(),
                "Error",
                "Please enter a single number")
            return

        points = zip(numpy.linspace(self.startPoint.x(), self.endPoint.x(), num_points),
                       numpy.linspace(self.startPoint.y(), self.endPoint.y(), num_points))

        self.layer.startEditing()
        self.layer.editingStopped.connect(self.unsetButton)

        self.layer.beginEditCommand("Transect started")
        for coord in points:
            newPoint = QgsFeature()
            newPoint.setGeometry(QgsGeometry.fromPointXY(QgsPointXY(coord[0], coord[1])))
            attrs = [None] * len(self.layer.fields())
            newPoint.setAttributes(attrs)
            self.layer.addFeatures([newPoint])
        self.layer.endEditCommand()
        self.layer.updateExtents()
        self.layer.reload()
        self.canvas.refresh()

    def canvasMoveEvent(self, e):
        if not self.isEmittingPoint:
            return

        self.endPoint = self.toMapCoordinates(e.pos())
        self.showDisplacement(self.startPoint, self.endPoint)

    def showDisplacement(self, startPoint, endPoint):
        self.rubberBandDisplacement.reset(QgsWkbTypes.LineGeometry)
        point1 = QgsPointXY(startPoint.x(), startPoint.y())
        point2 = QgsPointXY(endPoint.x(), endPoint.y())
        self.rubberBandDisplacement.addPoint(point1, False)
        self.rubberBandDisplacement.addPoint(
            point2, True)  # true to update canvas
        self.rubberBandDisplacement.show()


class ImportFileTool(QgsMapToolEmitPoint):
    def __init__(self, iface):
        self.iface = iface
        self.canvas = iface.mapCanvas()
        QgsMapToolEmitPoint.__init__(self, self.canvas)
        self.reset()

    def setLayer(self, layer):
        self.layer = layer

    def reset(self):
        self.layer = None

    def unsetButton(self):
        self.deactivate()
        self.canvas.unsetMapTool(self)
        self.reset()

    def canvasReleaseEvent(self, e):
        if self.sample_ids:
            clickedPoint = self.toMapCoordinates(e.pos())
            newPoint = QgsFeature()
            newPoint.setGeometry(QgsGeometry.fromPointXY(clickedPoint))

            cur_id = self.sample_ids.pop(0)
            attrs = [None] * len(self.layer.fields())
            newPoint.setAttributes(attrs)

            # Add Sample ID
            f_index = self.layer.fields().indexFromName(self.selectedField)
            newPoint.setAttribute(f_index, cur_id)

            # Add appending values
            for field in self.layer.fields():
                if field.name() in self.csv_data:
                    f_index = self.layer.fields().indexFromName(field.name())
                    newPoint.setAttribute(f_index, self.csv_data[field.name()][0])
                    # Remove from value from dict
                    self.csv_data[field.name()].pop(0)

            self.layer.beginEditCommand("Point " + cur_id)
            self.layer.addFeatures([newPoint])
            self.layer.endEditCommand()
            self.layer.updateExtents()
            self.layer.reload()
            self.canvas.refresh()

            if self.sample_ids:
                self.iface.mainWindow().statusBar().showMessage(self.sample_ids[0])
            else:
                self.iface.mainWindow().statusBar().clearMessage()
                self.iface.messageBar().pushSuccess(u'Success', u'All points loaded.')
                self.unsetButton()

    def loadCSV(self):
        allFields = self.layer.fields()
        self.selectedField, ok = QInputDialog.getItem(
            None, "Select Field", "Select the field containing the sample ID:",
            allFields.names(), 0, False)
        if not ok:
            self.unsetButton()
            return
        if allFields[allFields.lookupField(self.selectedField)].type() == QVariant.String:
            file, filters = QFileDialog.getOpenFileName(None, None, None, "CSV Files (*.csv *.txt)")
            if not file:
                self.unsetButton()
                return
            try:
                with open(file, 'r') as f:
                    lines = [line.rstrip() for line in f]
                headers = [n for n in lines[0].split(',')]
                self.sample_ids = []
                self.csv_data = {headers[n]: [] for n in range(len(headers))[1:]}
                for line in lines[1:]:
                    self.sample_ids.append(line.split(',')[0])
                    for n in range(len(headers))[1:]:
                        self.csv_data[headers[n]].append(line.split(',')[n])
            except:
                QMessageBox.information(
                    self.iface.mainWindow(),
                    "Error",
                    ".csv or .txt in wrong format.")
                self.unsetButton()
                return
        else:
            QMessageBox.information(
                self.iface.mainWindow(),
                "Error",
                "A text field must be selected.")
            self.unsetButton()
            return
        self.layer.startEditing()
        self.layer.editingStopped.connect(self.unsetButton)
        self.iface.mainWindow().statusBar().showMessage(self.sample_ids[0])