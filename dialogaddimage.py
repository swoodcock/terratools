# -*- coding: utf-8 -*-

import os.path

from PyQt5.QtWidgets import QDialog, QFileDialog, QMessageBox
from qgis.core import QgsProject

from .ui_dialogaddimage import Ui_DialogAddImage
from . import utils


class AddImageDialog(QDialog,
                     Ui_DialogAddImage):

    def __init__(self):
        QDialog.__init__(self)
        self.setupUi(self)

        self.pushButtonBrowse.clicked.connect(self.showBrowserDialog)

    def clear(self):
        self.lineEditImagePath.setText("")

    def showBrowserDialog(self):
        bDir, found = QgsProject.instance().readEntry(
            utils.SETTINGS_KEY, utils.SETTING_BROWSER_RASTER_DIR, None)
        if not found:
            bDir = os.path.expanduser("~")

        filepath, _ = QFileDialog.getOpenFileName(
            self, "Select image", bDir, "Images (*.tif *.png *.jpg *.pdf *.tiff *.bmp *.jpeg)")

        if filepath:
            self.lineEditImagePath.setText(filepath)
            bDir, _ = os.path.split(filepath)
            QgsProject.instance().writeEntry(utils.SETTINGS_KEY,
                                             utils.SETTING_BROWSER_RASTER_DIR,
                                             bDir)

    def accept(self):
        result, message, details = self.validate()
        if result:
            self.done(QDialog.Accepted)
        else:
            msgBox = QMessageBox()
            msgBox.setWindowTitle("Error")
            msgBox.setText(message)
            msgBox.setDetailedText(details)
            msgBox.setStandardButtons(QMessageBox.Ok)
            msgBox.exec_()

    def validate(self):
        result = True
        message = ""
        details = ""

        self.imagePath = self.lineEditImagePath.text()
        _, extension = os.path.splitext(self.imagePath)
        extension = extension.lower()
        if not os.path.isfile(self.imagePath) or \
                (extension not in [".tif", ".png", ".jpg", ".pdf", ".tiff", ".bmp", ".jpeg"]):
            result = False
            if len(details) > 0:
                details += "\n"
            details += "Must be an image file or pdf"

        if not result:
            message = "Errors in the directory or filename"

        return result, message, details
