# -*- coding: utf-8 -*-

import os.path

from PyQt5.QtWidgets import QDialog, QFileDialog, QMessageBox

from .ui_dialogexportgeorast import Ui_DialogExportGeoRast


class ExportGeoRastDialog(QDialog,
                          Ui_DialogExportGeoRast):

    def __init__(self):
        QDialog.__init__(self)
        self.setupUi(self)

        self.pushButtonBrowse.clicked.connect(self.showBrowserDialog)

    def clear(self, layer):
        self.lineEditImagePath.setText("")
        self.checkBoxRotationMode.setChecked(False)

        defaultPath, _ = os.path.splitext(layer.filepath)
        self.defaultPath = defaultPath + "_georef.tif"

    def showBrowserDialog(self):
        if self.lineEditImagePath.text():
            filepathDialog = self.lineEditImagePath.text()
        else:
            filepathDialog = self.defaultPath

        filepath, _ = QFileDialog.getSaveFileName(
            None, "Export georeferenced raster", filepathDialog,
            "Images (*.tif *.png *.jpg *.tiff *.bmp *.jpeg)")

        if filepath:
            self.lineEditImagePath.setText(filepath)

    def accept(self):
        result, message, details = self.validate()
        if result:
            self.done(QDialog.Accepted)
        else:
            msgBox = QMessageBox()
            msgBox.setWindowTitle("Error")
            msgBox.setText(message)
            msgBox.setDetailedText(details)
            msgBox.setStandardButtons(QMessageBox.Ok)
            msgBox.exec_()

    def validate(self):
        result = True
        message = ""
        details = ""

        self.isPutRotationInWorldFile = self.checkBoxRotationMode.isChecked()

        self.imagePath = self.lineEditImagePath.text()
        if not self.imagePath:
            result = False
            details += "A file must be selected"

        if result:
            _, extension = os.path.splitext(self.imagePath)
            extension = extension.lower()
            if extension not in [".tif", ".png", ".jpg", ".tiff", ".bmp", ".jpeg"]:
                result = False
                if len(details) > 0:
                    details += "\n"
                details += "Must be an image file"

        if not result:
            message = "Errors in the directory or filename"

        return result, message, details
